package org.minbox.framework.logging.admin.sample;

import com.alibaba.fastjson.JSON;
import org.minbox.framework.logging.admin.storage.LoggingStorage;
import org.minbox.framework.logging.core.MinBoxLog;
import org.minbox.framework.logging.core.response.LoggingResponse;
import org.minbox.framework.logging.core.response.ServiceResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import java.sql.SQLException;
import java.util.List;

/**
 * @author：恒宇少年 - 于起宇
 * <p>
 * DateTime：2019/9/15 2:14 下午
 * Blog：http://blog.yuqiyu.com
 * WebSite：http://www.jianshu.com/u/092df3f77bca
 * Gitee：https://gitee.com/hengboy
 * GitHub：https://github.com/hengboy
 */
@Component
public class CustomerLoggingStorage implements LoggingStorage {
    /**
     * logger instance
     */
    static Logger logger = LoggerFactory.getLogger(CustomerLoggingStorage.class);

    @Override
    public void insertLog(String serviceDetailId, MinBoxLog log) throws SQLException {
        logger.info("服务编号：{}，日志：{}", serviceDetailId, JSON.toJSONString(log));
    }

    @Override
    public String insertServiceDetail(String serviceId, String serviceIp, int servicePort) throws SQLException {
        logger.info("服务编号：{}，服务IP地址：{}，服务端口号：{}", serviceId, serviceIp, servicePort);
        return null;
    }

    @Override
    public String selectServiceDetailId(String serviceId, String serviceIp, int servicePort) throws SQLException {
        logger.info("服务编号：{}，服务IP地址：{}，服务端口号：{}", serviceId, serviceIp, servicePort);
        return null;
    }

    @Override
    public List<ServiceResponse> findAllService() throws SQLException {
        return null;
    }

    @Override
    public List<LoggingResponse> findTopList(int topCount) throws SQLException {
        return null;
    }

    @Override
    public void updateLastReportTime(String serviceDetailId) throws SQLException {

    }
}
