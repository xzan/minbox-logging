package org.minbox.framework.logging.client.sample;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 恒宇少年
 */
@RestController
public class TestController {

    @GetMapping(value = "/test")
    public String test() {
        return "测试日志接口";
    }
}
